// Za združljivost razvoja na lokalnem računalniku ali Cloud9 okolju
if (process.env.PORT) {
    process.env.PORT = 8080;
}

var http = require('http');
var steviloUporabnikov = 0;
http.createServer(function(zahteva, odgovor) {
    steviloUporabnikov++;
    odgovor.writeHead(200, {'Content-Type': 'text/plain'});
    odgovor.end('Pozdravljeni ' + steviloUporabnikov + '. ljubitelj predmeta OIS!\n');
}).listen(process.env.PORT);

console.log('Strežnik je pognan.');